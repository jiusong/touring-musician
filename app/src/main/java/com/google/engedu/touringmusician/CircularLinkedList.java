/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.touringmusician;


import android.graphics.Point;

import java.util.Iterator;

public class CircularLinkedList implements Iterable<Point> {

    public class Node {
        Point point;
        Node prev, next;
        /**
         **
         **  YOUR CODE GOES HERE
         **
         **/
    }

    Node head;

    public void insertBeginning(Point p) {
        /**
         **
         **  YOUR CODE GOES HERE
         **
         **/
        Node node = new Node();
        node.point = p;
        if (head == null) {
            head = node;
            head.prev = head;
            head.next = head;
        } else {
            Node last = head.prev;
            head.prev = node;
            node.next = head;
            node.prev= last;
            last.next = node;
        }
    }

    private float distanceBetween(Point from, Point to) {
        return (float) Math.sqrt(Math.pow(from.y-to.y, 2) + Math.pow(from.x-to.x, 2));
    }

    public float totalDistance() {
        float total = 0;
        /**
         **
         **  YOUR CODE GOES HERE
         **
         **/
        Node cur = head;
        Node next = head.next;
        while (next != head) {
            total += distanceBetween(cur.point, next.point);
            cur = next;
            next = next.next;
        }
        return total;
    }

    public void insertNearest(Point p) {
        /**
         **
         **  YOUR CODE GOES HERE
         **
         **/
        if (head == null) {
            head = new Node();
            head.point = p;
            head.prev = head;
            head.next = head;
        } else {
            Node closestNode = head;
            float shortestDistance = distanceBetween(head.point, p);
            Node node = head.next;
            while (node != head) {
                float distance = distanceBetween(head.point, p);
                if (shortestDistance < distance) {
                    shortestDistance = distance;
                    closestNode = node;
                }
                node = node.next;
            }
            Node newNode = new Node();
            newNode.point = p;
            Node next = closestNode.next;
            closestNode.next = newNode;
            newNode.prev= closestNode;
            newNode.next = next;
            next.prev = newNode;
        }
    }

    public void insertSmallest(Point p) {
        /**
         **
         **  YOUR CODE GOES HERE
         **
         **/
        Node newNode = new Node();
        newNode.point = p;
        if (head == null) {
            head = newNode;
            head.point = p;
            head.prev = head;
            head.next = head;
        }  else if (head.next == head) {
            head.next = newNode;
            head.prev = newNode;
            newNode.prev = head;
            newNode.next = head;
        } else {
            Node best = head;
            Node node = head;
            Node next = head.next;
            float d1 = distanceBetween(node.point, next.point);
            float d2 = distanceBetween(node.point, p) + distanceBetween(next.point, p);
            float distance = d2 - d1;
            while (next != head) {
                d1 = distanceBetween(next.point, next.next.point);
                d2 = distanceBetween(next.point, p) + distanceBetween(next.next.point, p);
                if (d2 - d1 < distance) {
                    distance = d2 - d1;
                    best = next;
                }
                next = next.next;
            }
            next = best.next;
            best.next = newNode;
            next.prev = newNode;
            newNode.next = next;
        }

    }

    public void reset() {
        head = null;
    }

    private class CircularLinkedListIterator implements Iterator<Point> {

        Node current;

        public CircularLinkedListIterator() {
            current = head;
        }

        @Override
        public boolean hasNext() {
            return (current != null);
        }

        @Override
        public Point next() {
            Point toReturn = current.point;
            current = current.next;
            if (current == head) {
                current = null;
            }
            return toReturn;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public Iterator<Point> iterator() {
        return new CircularLinkedListIterator();
    }


}
